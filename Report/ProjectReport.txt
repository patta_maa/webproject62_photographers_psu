Project: Photographers_PSU
Member: 1.Nurulmuminah Jehdeng 6110210235  2.Pattamawan Chusuwan 6110210256
Problem:During the festival, whether it be the graduation of the university, etc. 
        students or associates want to come to find a photographer nearby
        For convenience So this website was created to be able to find 
        photographers near PSU and photographers can promote themselves too.
Proposal:It is a website that collects photographers around PSU for 
         users who are searching for photographers. And also allowing the 
         photographer to promote themselves through our website.
Technology Used:
   - dotnet core 2.1
   - SQLite
   - Bootstrap Framework
   - css
List of Features (Prototype)
   - Membership signing up
   - Add information to promote photographers by each member
   - Front part for displaying information of photographers, 
     page for promoting photographers
List of Features (Complete Version)
   - Users can comment on website reviews.

