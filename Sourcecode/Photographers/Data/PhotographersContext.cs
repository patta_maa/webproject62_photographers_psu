﻿using Photographers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Photographers.Data
{
    public class PhotographersContext : IdentityDbContext<NewsUser>
    {
    public DbSet<Photo> newsPhoto { get; set; }
    

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(@"Data source=Photo.db");
    }
    }
 }