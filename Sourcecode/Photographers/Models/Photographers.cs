﻿using System;
using System.ComponentModel.DataAnnotations;using Microsoft.AspNetCore.Identity;namespace Photographers.Models
{
    public class NewsUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class Photo {

        public int PhotoID { get; set; }
        public string PicPhoto {get; set;}
        public string ShotName { get; set; }
        public string fullName { get; set; }

        public string occupationID { get; set; }
        public string Gender { get; set; }
        public string Tell { get; set; }

        public string Facebook { get; set; }
        public string ExamplePhoto { get; set; }
        public string ReviewPhotographer { get; set; }

        [DataType(DataType.Date)]
        public string UpdateDate { get; set; }

        public string NewsUserId { get; set; }
        public NewsUser postUser { get; set; }





    }

}