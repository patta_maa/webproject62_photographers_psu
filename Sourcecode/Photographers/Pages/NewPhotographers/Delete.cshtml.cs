using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Photographers.Data;
using Photographers.Models;

namespace Photographers.Pages.NewPhotographers
{
    public class DeleteModel : PageModel
    {
        private readonly Photographers.Data.PhotographersContext _context;

        public DeleteModel(Photographers.Data.PhotographersContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Photo Photo { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Photo = await _context.newsPhoto
                .Include(p => p.postUser).FirstOrDefaultAsync(m => m.PhotoID == id);

            if (Photo == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Photo = await _context.newsPhoto.FindAsync(id);

            if (Photo != null)
            {
                _context.newsPhoto.Remove(Photo);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
