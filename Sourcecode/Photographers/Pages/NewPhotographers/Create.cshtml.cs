using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Photographers.Data;
using Photographers.Models;

namespace Photographers.Pages.NewPhotographers
{
    public class CreateModel : PageModel
    {
        private readonly Photographers.Data.PhotographersContext _context;

        public CreateModel(Photographers.Data.PhotographersContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Photo Photo { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsPhoto.Add(Photo);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}