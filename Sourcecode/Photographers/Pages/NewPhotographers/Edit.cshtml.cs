using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Photographers.Data;
using Photographers.Models;

namespace Photographers.Pages.NewPhotographers
{
    public class EditModel : PageModel
    {
        private readonly Photographers.Data.PhotographersContext _context;

        public EditModel(Photographers.Data.PhotographersContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Photo Photo { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Photo = await _context.newsPhoto
                .Include(p => p.postUser).FirstOrDefaultAsync(m => m.PhotoID == id);

            if (Photo == null)
            {
                return NotFound();
            }
           ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Photo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhotoExists(Photo.PhotoID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool PhotoExists(int id)
        {
            return _context.newsPhoto.Any(e => e.PhotoID == id);
        }
    }
}
