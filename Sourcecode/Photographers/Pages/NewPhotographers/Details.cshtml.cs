using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Photographers.Data;
using Photographers.Models;

namespace Photographers.Pages.NewPhotographers
{
    public class DetailsModel : PageModel
    {
        private readonly Photographers.Data.PhotographersContext _context;

        public DetailsModel(Photographers.Data.PhotographersContext context)
        {
            _context = context;
        }

        public Photo Photo { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Photo = await _context.newsPhoto
                .Include(p => p.postUser).FirstOrDefaultAsync(m => m.PhotoID == id);

            if (Photo == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
