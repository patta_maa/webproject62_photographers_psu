﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Photographers.Data;
using Photographers.Models;

namespace Photographers.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Photographers.Data.PhotographersContext _context;

        public IndexModel(Photographers.Data.PhotographersContext context)
        {
            _context = context;
        }

        public IList<Photo> Photo { get; set; }

        public async Task OnGetAsync()
        {
            Photo = await _context.newsPhoto
                  .Include(p => p.postUser).ToListAsync();
        }
    }
}
